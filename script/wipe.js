        $(document).ready(function () {

            var controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave'
                }
            });

            // get all slides
            var slides = document.querySelectorAll(".wipes");

            // create scene for every slide
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i])
                    .addTo(controller);
            }

            let menuButton = new ScrollMagic.Scene({
                triggerElement: '.contact'

            })

            .setClassToggle('.home__menuButtonItem', 'color')

            .addTo(controller);

            let homeButton = new ScrollMagic.Scene({
                triggerElement: '.contact'

            })

            .setClassToggle('.home__homeButton', 'background')

            .addTo(controller);
            
            
            let topLine = new ScrollMagic.Scene({
                triggerElement: '.contact'

            })

            .setClassToggle('.contact__mail', 'animation')

            .addTo(controller);
                        
            let bottomLine = new ScrollMagic.Scene({
                triggerElement: '.contact'

            })

            .setClassToggle('.contact__bottomLine', 'animation')

            .addTo(controller);
            
            



        });
